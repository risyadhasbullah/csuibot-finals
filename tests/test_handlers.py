from unittest.mock import Mock

from csuibot.handlers import help, zodiac, shio, recipe, similar


def test_help(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock()

    help(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (
        'CSUIBot v0.0.1\n\n'
        'Dari Fasilkom, oleh Fasilkom, untuk Fasilkom!'
    )
    assert args[1] == expected_text


def test_zodiac(mocker):
    fake_zodiac = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_zodiac', return_value=fake_zodiac)
    mock_message = Mock(text='/zodiac 2015-05-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_zodiac


def test_zodiac_invalid_month_or_day(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/zodiac 2015-25-05')

    zodiac(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Month or day is invalid'


def test_shio(mocker):
    fake_shio = 'foo bar'
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_chinese_zodiac', return_value=fake_shio)
    mock_message = Mock(text='/shio 2015-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == fake_shio


def test_shio_invalid_year(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mocker.patch('csuibot.handlers.lookup_chinese_zodiac', side_effect=ValueError)
    mock_message = Mock(text='/shio 1134-05-05')

    shio(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Year is invalid'


def test_invalid_recipe(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='/recipe cacinganbelummandi')

    recipe(mock_message)

    args, _ = mocked_reply_to.call_args
    assert args[1] == 'Sorry we could not find any recipe.'


def test_recipe(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='/recipe spaghetti')

    recipe(mock_message)

    args, _ = mocked_reply_to.call_args
    assert 'We use API from edamam. All rights reserved to edamam' in args[1]


def test_similar(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')
    mock_message = Mock(text='/recipe spaghetti')
    recipe(mock_message)
    mock_message = Mock(text='/similar recipe')
    similar(mock_message)

    args, _ = mocked_reply_to.call_args
    assert 'We use API from edamam. All rights reserved to edamam' in args[1]


def test_555_error(mocker):
    mocked_reply_to = mocker.patch('csuibot.handlers.bot.reply_to')

    class fakeResponse:
        status_code = 555

    mocker.patch('csuibot.handlers.requests.get', return_value=fakeResponse())
    mock_message = Mock(text='/recipe spaghetti')

    recipe(mock_message)

    args, _ = mocked_reply_to.call_args
    assert 'Low quality recipe, please use another one' in args[1]
