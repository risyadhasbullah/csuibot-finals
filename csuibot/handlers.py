from . import app, bot
from .utils import lookup_zodiac, lookup_chinese_zodiac, view_lists, insert, create_list
import requests
import json
recipe_ingredient = None
index_flag = None
invalid_flag = 1


@bot.message_handler(regexp=r'^/about$')
def help(message):
    app.logger.debug("'about' command detected")
    about_text = (
        'CSUIBot v0.0.1\n\n'
        'Dari Fasilkom, oleh Fasilkom, untuk Fasilkom!'
    )
    bot.reply_to(message, about_text)


@bot.message_handler(regexp=r'^/zodiac \d{4}\-\d{2}\-\d{2}$')
def zodiac(message):
    app.logger.debug("'zodiac' command detected")
    _, date_str = message.text.split(' ')
    _, month, day = parse_date(date_str)
    app.logger.debug('month = {}, day = {}'.format(month, day))

    try:
        zodiac = lookup_zodiac(month, day)
    except ValueError:
        bot.reply_to(message, 'Month or day is invalid')
    else:
        bot.reply_to(message, zodiac)


@bot.message_handler(regexp=r'^/shio \d{4}\-\d{2}\-\d{2}$')
def shio(message):
    app.logger.debug("'shio' command detected")
    _, date_str = message.text.split(' ')
    year, _, _ = parse_date(date_str)
    app.logger.debug('year = {}'.format(year))

    try:
        zodiac = lookup_chinese_zodiac(year)
    except ValueError:
        bot.reply_to(message, 'Year is invalid')
    else:
        bot.reply_to(message, zodiac)


@bot.message_handler(regexp=r'^/recipe \S+$')
def recipe(message):
    app.logger.debug("'recipe' command detected")
    global recipe_ingredient
    url = 'https://api.edamam.com/search?q='
    ingredient = message.text.split(' ')
    recipe_ingredient = str(ingredient[1])
    url += str(ingredient[1])
    url += '&app_id=07fcb4d8'
    url += '&app_key=1f1f6ff18c2fe9e943269b16cbd3f82d'
    url += '&from=1&to=2'
    resp = requests.get(url)
    if resp.status_code == 555:
        replyMessage = 'Low quality recipe, please use another one'
        return bot.reply_to(message, replyMessage)
    resp.encoding = 'UTF-8'
    data = json.loads(resp.text)
    count = data["count"]
    if count == 0:
        replyMessage = 'Sorry we could not find any recipe.'
        bot.reply_to(message, replyMessage)
    else:
        replyMessage = data["hits"][0]["recipe"]["label"] + "\n"
        replyMessage += data["hits"][0]["recipe"]["url"] + "\n"
        for ingredient in data["hits"][0]["recipe"]["ingredientLines"]:
            replyMessage += ingredient + "\n"
        replyMessage += 'We use API from edamam. All rights reserved to edamam'
        bot.reply_to(message, replyMessage)


@bot.message_handler(regexp=r'^/similar recipe$')
def similar(message):
    app.logger.debug("'recipe' command detected")
    global recipe_ingredient
    url = 'https://api.edamam.com/search?q='
    ingredient = message.text.split(' ')
    url += str(recipe_ingredient)
    url += '&app_id=07fcb4d8'
    url += '&app_key=1f1f6ff18c2fe9e943269b16cbd3f82d'
    url += '&from=2&to=3'
    resp = requests.get(url)
    resp.encoding = 'UTF-8'
    data = json.loads(resp.text)
    replyMessage = data["hits"][0]["recipe"]["label"] + "\n"
    replyMessage += data["hits"][0]["recipe"]["url"] + "\n"
    for ingredient in data["hits"][0]["recipe"]["ingredientLines"]:
        replyMessage += ingredient + "\n"
    replyMessage += 'We use API from edamam. All rights reserved to edamam'
    bot.reply_to(message, replyMessage)


@bot.message_handler(regexp=r'^/create_list \S+\s*\S*$')
def create(message):
    app.logger.debug("create list command detected")
    if message.chat.type == "private":
        replyMessage = "Sorry this feature can only be use in group chat"
        return bot.reply_to(message, replyMessage)
    else:
        list_name = message.text.split(' ')
        if len(list_name) == 3:
            list_name = list_name[1] + ' ' + list_name[2]
        else:
            list_name = list_name[1]
        replyMessage = create_list(list_name)
        bot.reply_to(message, replyMessage)


@bot.message_handler(regexp=r'^/view_list$')
def view(message):
    app.logger.debug("create list command detected")
    if message.chat.type != "group":
        replyMessage = "Sorry this feature can only be use in group chat"
        return bot.reply_to(message, replyMessage)
    else:
        replyMessage = view_lists()
        bot.reply_to(message, replyMessage)


@bot.message_handler(regexp=r'^/add_todo [0-9]{1,2}$')
def ask_todo(message):
    app.logger.debug("ask todo command detected")
    if message.chat.type != "group":
        replyMessage = "Sorry this feature can only be use in group chat"
        return bot.reply_to(message, replyMessage)
    else:
        idx = message.text.split(' ')
        idx = int(idx[1]) - 1
        global index_flag
        global invalid_flag
        index_flag = idx
        invalid_flag = 0
        replyMessage = "What activity do you want to add?"
        msg = bot.reply_to(message, replyMessage)
        bot.register_next_step_handler(msg, insert_todo)


@bot.message_handler(func=lambda message: True)
def insert_todo(message):
    app.logger.debug("insert command detected")
    todo_name = message.text
    global invalid_flag
    global index_flag
    invalid_flag = 1
    replyMessage = insert(index_flag, todo_name)
    bot.reply_to(message, replyMessage)


@bot.message_handler(func=lambda message: True)
def get_all(message):
    if invalid_flag == 1:
        bot.reply_to(message, "Invalid command")


def parse_date(text):
    return tuple(map(int, text.split('-')))
