from csuibot.utils import zodiac as z
import os.path
import json


def lookup_zodiac(month, day):
    zodiacs = [
        z.Aries(),
        z.Taurus(),
        z.Gemini(),
        z.Cancer(),
        z.Leo(),
        z.Virgo(),
        z.Libra(),
        z.Scorpio(),
        z.Sagittarius(),
        z.Capricorn(),
        z.Aquarius(),
        z.Pisces()
    ]

    for zodiac in zodiacs:
        if zodiac.date_includes(month, day):
            return zodiac.name
    else:
        return 'Unknown zodiac'


def lookup_chinese_zodiac(year):
    num_zodiacs = 12
    zodiacs = {
        0: 'rat',
        1: 'buffalo',
        2: 'tiger',
        3: 'rabbit',
        4: 'dragon',
        5: 'snake',
        6: 'horse',
        7: 'goat',
        8: 'monkey'
    }
    ix = (year - 4) % num_zodiacs

    try:
        return zodiacs[ix]
    except KeyError:
        return 'Unknown zodiac'


def openFile():
    try:
        filename = os.path.join(os.path.expanduser("~"), "storagedata.json")
        with open(filename, "r") as jsonFile:
            data = json.load(jsonFile)
            return data
    except OSError:
        return list()


def writeFile(data):
    filename = os.path.join(os.path.expanduser("~"), "storagedata.json")
    with open(filename, "w") as jsonFile:
        json.dump(data, jsonFile, indent=4)


def create_list(list_name):
    data = openFile()
    selected_list = checkList(data, list_name)
    if selected_list is None:
        idx = len(data)
        data.append(list())
        data[idx].append(list_name)
        writeFile(data)
        return "List {} has been created".format(str(list_name))
    else:
        return "Sorry you already have that list"


def insert(list_idx, todo):
    data = openFile()
    selected_list = getList(data, list_idx)
    if selected_list is None:
        return "Sorry the list does not exist"
    else:
        data[list_idx].append(str(todo))
        writeFile(data)
        return "{} has been inserted".format(todo)


def view_lists():
    data = openFile()
    list_name = getListName(data)
    if len(data) == 0:
        return "Sorry you don't have any list"
    else:
        reply = ''
        i = 1
        for lists in list_name:
            reply += '{} {} \n'.format(i, lists)
            i += 1
        return reply


def del_lists(list_idx):
    data = openFile()
    del data[list_idx]
    writeFile(data)        


def del_todo(list_idx, todo):
    data = openFile()
    data[list_idx].remove(str(todo))
    writeFile(data)
    return "{} has been removed".format(todo)


def view_todos(list_idx):
    data = openFile()
    selected_list = getList(data, list_idx)
    if selected_list is None:
        return "Sorry the list does not exist"
    else:
        i = 0
        reply = "List : {} \n".format(selected_list[0])
        for todos in selected_list:
            if i == 0:
                i += 1
            else:
                reply += "{} {} \n".format(i, todos)
        return reply


def getList(data, list_idx):
    try:
        selected_list = data[list_idx]
        return selected_list
    except (UnboundLocalError, TypeError, KeyError):
        return None


def getListName(data):
    try:
        list_name = []
        for lists in data:
            list_name.append(lists[0])
        return list_name
    except (UnboundLocalError, TypeError, KeyError):
        return None


def checkList(data, list_name):
    try:
        for lists in data:
            if lists[0] == list_name:
                return "Sorry you already have that list"
        return None
    except (UnboundLocalError, TypeError, KeyError):
        return None
